# Copyright 1999-2018 Gentoo Authors
# Distributed under the terms of the GNU General Public License v2

EAPI=6
inherit golang-build golang-vcs-snapshot bash-completion-r1

KEYWORDS="~amd64"
EGO_PN="github.com/hashicorp/terraform"
ARCHIVE_URI="https://github.com/hashicorp/terraform/archive/v${PV}.tar.gz -> ${P}.tar.gz"

DESCRIPTION="Write, Plan and Create Infrastructure as Code"
HOMEPAGE="https://github.com/hashicorp/terraform https://www.terraform.io/"
SRC_URI="${ARCHIVE_URI}"

LICENSE="MPL-2.0"
SLOT="0"
IUSE=""

RESTRICT="test"

DEPEND="dev-go/gox
	>=dev-lang/go-1.9:=
	dev-go/go-tools:="
REDEPEND=""

src_install() {
	dobin "${PN}"
}
