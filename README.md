<!-- START doctoc generated TOC please keep comment here to allow auto update -->
<!-- DON'T EDIT THIS SECTION, INSTEAD RE-RUN doctoc TO UPDATE -->
**Table of Contents**  *generated with [DocToc](https://github.com/thlorenz/doctoc)*

- [fixl's custom overlay](#fixls-custom-overlay)
  - [Usage](#usage)
  - [Contribute](#contribute)
  - [Acknowledgements](#acknowledgements)

<!-- END doctoc generated TOC please keep comment here to allow auto update -->

# fixl's custom overlay

Made with love by Stefan, a.k.a [fixl](https://fixl.info).

## Usage

First install `layman` from the official Portage tree. You must set the use-flag `USE="git"` when you install it.

```bash
emerge layman
```

Then, in the section "overlays" in `/etc/layman/layman.cfg`, add the following url: https://gitlab.com/fixl/gentoo/raw/master/overlay.xml

Now run the following command:

```bash
layman --fetch --overlays https://gitlab.com/fixl/gentoo/raw/master/overlay.xml --add fixl
```

And you're done.

To refresh the overlay (and any other overlay you added):

```bash
layman -S

# Or the long form
layman --sync-all
```

Viel Spass!

## Contribute

Contributions are welcome. Please follow Gentoo's [commit message format](https://devmanual.gentoo.org/ebuild-maintenance/git/index.html#git-commit-message-format).

## Acknowledgements

Thanks to [Jorgicio](http://www.jorgicio.net/), who's [overlay](https://github.com/jorgicio/jorgicio-gentoo/blob/master/README.md) was a great deal of help to get my own one going :)
