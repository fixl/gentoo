# Copyright 1999-2018 Gentoo Foundation
# Distributed under the terms of the GNU General Public License v2

EAPI=5

inherit bash-completion-r1

MY_PN="todo.txt_cli"
MY_P="todo.txt_cli-${PV}"
S="${WORKDIR}/${MY_P}"

DESCRIPTION="A simple and extensible shell script for managing your todo.txt file."
HOMEPAGE="http://todotxt.com"
SRC_URI="https://github.com/todotxt/todo.txt-cli/releases/download/v${PV}.0/${MY_P}.tar.gz"

LICENSE="GPL-3"
SLOT="0"
KEYWORDS="~amd64"
IUSE="bash-completion"

DEPEND=""
RDEPEND="${DEPEND}
		app-shells/bash"

src_install() {
	dobin todo.sh  || die
	dodoc todo.cfg || die

	use bash-completion && newbashcomp todo_completion todotxt
}
